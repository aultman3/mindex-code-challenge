
package com.mindex.challenge.service.impl;

import com.mindex.challenge.dao.EmployeeRepository;
import com.mindex.challenge.data.Employee;
import com.mindex.challenge.service.ReportingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class ReportingServiceImpl implements ReportingService {

    private static final Logger LOG = LoggerFactory.getLogger(ReportingServiceImpl.class);

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public HashMap read(String id) {
        LOG.debug("Reading reporting with id [{}]", id);
        HashMap<String, Object> map = new HashMap<>();
        Employee employee = employeeRepository.findByEmployeeId(id);

        if (employee == null) {
            throw new RuntimeException("Invalid employeeId: " + id);
        }


        int numberOfReports = incrementNumberOfReports(employee);

        map.put("employee", employee);
        map.put("numberOfReports", numberOfReports);

        return map;
    }

    private int incrementNumberOfReports(Employee employee){
        int numberOfReports = 0;
        if( employee.getDirectReports() != null) {
            for (Employee report : employee.getDirectReports()) {
                numberOfReports++;
                Employee fullReport = employeeRepository
                        .findByEmployeeId(report.getEmployeeId());
                numberOfReports += incrementNumberOfReports(fullReport);
            }
        }
        return numberOfReports;
    }

}
