package com.mindex.challenge.data;

import java.util.Date;

public class Compensation extends Employee{
    private float salary;
    private Date effectiveDate;

    public Compensation() {
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }


}
